var AdTech = function (options) {
    var settings = $.extend({

    }, options);
    var self;
    return {
        init: function () {
            $(window).resize(function () {
                var width = getWindowWidth();
                var widthLastSet = $("#browserwidth").val();
                if (width <= 767) {
                    $("#browserwidth").val(767);
                }
                else if (width <= 970 && width >= 768) {
                    $("#browserwidth").val(970);
                }
                else if (width <= 1240 && width > 971) {
                    $("#browserwidth").val(1240);
                }
                else if (width <= 1483 && width > 1241) {
                    $("#browserwidth").val(1483);
                }
                else if (width > 1484) {
                    $("#browserwidth").val(1484);
                }
                if (widthLastSet != $("#browserwidth").val()) {
                    if (width <= 767) {
                        showMobileTakeOver();
                    }
                    else if (width <= 970 && width >= 768) {
                        showTabletTakeOver();
                    }
                    if (width <= 1240 && width > 971) {
                        showSmallDesktopTakeOver();
                    }
                    else if (width <= 1483 && width > 1241) {
                        showSmallDesktopTakeOver();
                    }
                    else if (width > 1484) {
                        showLargeDesktopTakeOver();
                    }
                }
            });
        }
    };
};
var adTech;
$(function () {
    adTech = new AdTech().init();
});

function getWindowWidth() {
    return $(window).width();
}

function getWindowHeight() {
    return $(window).height();
}

function showMobileTakeOver() {
    $("#takeOverTop").html("<div style='background:#fff;text-align:center;margin:0 auto;border-bottom:1px solid #d8d8d8;'><img src='mobile.gif'/></div>");
    $("#takeOverLeft").html("");
    $("#takeOverRight").html("");
    $("#mainContent").removeClass().addClass('container mobileContent');
    $("#takeOverMain").removeClass().addClass('container padding-left-right-0 mobileContent');
    $(".nav-tabs li a").attr("style", "padding:5px 15px 5px 15px!important;");
    $("#leaderboard").html("<div style='background:#fff;text-align:center;margin:0 auto;border-bottom:1px solid #d8d8d8;padding-bottom:15px;'><img src='mobile.gif'/></div>");
    $("#mostleft").removeClass().addClass("left hidden-md");
}

function showTabletTakeOver() {
    $("#takeOverTop").html("<div style='border-bottom:1px solid #d8d8d8;'><img src='ipadtop.gif' class='img-responsive'/></div>");
    $("#takeOverLeft").html("");
    $("#takeOverRight").html("");
    $("#mainContent").removeClass().addClass('container tabletContent');
    $("#takeOverMain").removeClass().addClass('container padding-left-right-0 tabletContent');
    $(".nav-tabs li a").attr("style", "padding:5px 15px 5px 15px!important;");
    $("#mostleft").removeClass().addClass("left hidden-md");
}

function showSmallDesktopTakeOver() {
    $("#takeOverTop").html("<div style='height:101px;border-bottom:1px solid #d8d8d8;'><embed width=\"970\" height=\"100\" src=\"topsmall.swf\"></div>");
    $("#takeOverLeft").html("<div style=\"position:absolute;left:-105px;top:0;\"><embed width=\"105\" height=\"818\" src=\"leftsmall.swf\"></div>");
    $("#takeOverRight").html("<div style=\"position:absolute;right:-105px;top:0;\"><embed width=\"105\" height=\"818\" src=\"rightsmall.swf\"></div>");
    $("#mainContent").removeClass().addClass('container smallDesktopContent');
    $("#takeOverMain").removeClass().addClass('container padding-left-right-0 smallDesktopContent');
    $(".nav-tabs li a").attr("style", "padding:5px 7px 5px 5px!important;");
    $("#mostleft").removeClass().addClass("left hidden-md hidden-lg");
}

function showLargeDesktopTakeOver() {
    $("#takeOverTop").html("<div style='height:101px;border-bottom:1px solid #d8d8d8;'><embed width=\"1170\" height=\"100\" src=\"top.swf\"></div>");
    $("#takeOverLeft").html("<div style=\"position:absolute;left:-167px;top:0;\"><embed width=\"167\" height=\"1054\" src=\"left.swf\"></div>");
    $("#takeOverRight").html("<div style=\"position:absolute;right:-167px;top:0;\"><embed width=\"167\" height=\"1054\" src=\"right.swf\"></div>");
    $("#mainContent").removeClass().addClass('container largeDesktopContent');
    $("#takeOverMain").removeClass().addClass('container padding-left-right-0 largeDesktopContent');
    $(".nav-tabs li a").attr("style", "padding:5px 15px 5px 15px!important;");
    $("#mostleft").removeClass().addClass("left hidden-md");
}
