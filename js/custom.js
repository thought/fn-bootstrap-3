$(document).ready(function () {

    //clone mpu of 3rd column to display in first column
    $("#mpu3").clone().prependTo("#replacement1");

    //make same height for left & right (average fuel price)
    $("#right").attr("style", "height:" + $("#left").height() + "px;");

    //bootstrap tab event
    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

    //dropdown
    $('.dropdown-toggle').dropdown()

    //mobile main menu click event
    $(document).on("click", "#sidr ul li a", function (e) {
        e.preventDefault();
        $("#sidr ul").html('');
        $("#sidr2").html(getSubNavigation($(this).parent("li").html())).animate({
            marginLeft: "-260px",
            display: "block"
        }, 500);
    });

    //mobile submenu click event
    $(document).on("click", "#left", function (e) {
        e.preventDefault();
        $("#sidr").html(getRootNavigation());
    });

    //sites icon click event
    $(document).on("click", "#sites", function (e) {
        e.preventDefault();
        $("#sites-con").slideToggle("slow");
    });

    //search icon click event
    $(document).on("click", "#search", function (e) {
        e.preventDefault();
        $("#search-con").slideToggle("slow");
    });

    //get sub navigation
    function getSubNavigation(parentNavigation, parentText) {
        var currentUrl = $(parentNavigation).attr("href");
        var currentText = $(parentNavigation).text();
        return "<div id='sidr1' style='background:orange;width:260px;height:100%;position:absolute;left:0;display:none;'></div><ul><li><span class='m-left'><a href='' id='left'>&nbsp;<span class='glyphicon glyphicon-chevron-left' style='border:1px solid #ccc;padding:0 15px;'></span></a></span><span class='m-center'><a href='" + currentUrl + "'>" + currentText + "</a></span><span class='m-right'><a href='" + currentUrl + "' id='right'><span class='glyphicon glyphicon-th-list' style='border:1px solid #ccc;padding:0 15px;'></span></a></span><span class='clearfix'></span><ul class='sub'><li><a href='/about-us/'>Aboutus<span class='glyphicon glyphicon-chevron-right'></span></a></li><li><a href='/team/'>The Team<span class='glyphicon glyphicon-chevron-right'></span></a></li><li><a href='/team/'>Others<span class='glyphicon glyphicon-chevron-right'></span></a></li></ul></li></ul>";
    }

    //get main navigation
    function getRootNavigation() {
        return "<div id='sidr2' style='background:#ccc;width:260px;height:100%;position:absolute;left:260px;display:none;-moz-box-shadow:inset 0 0 10px rgba(0,0,0,0.3);-webkit-box-shadow: inset 0 0 10px rgba(0,0,0,0.3);box-shadow:inset 0 0 10px rgba(0,0,0,0.3);border-right: 1px solid #ccc;'></div><ul><li><a href=\"/home/\">Home<span class=\"glyphicon glyphicon-chevron-right\"></span></a></li><li><a href=\"/news/\">News<span class=\"glyphicon glyphicon-chevron-right\"></span></a></li><li><a href=\"/fleet-management/\">Cars<span class=\"glyphicon glyphicon-chevron-right\"></span></a></li><li><a href=\"/case-studies/\">Vans<span class=\"glyphicon glyphicon-chevron-right\"></span></a></li><li><a href=\"/faq/\">Suppliers<span class=\"glyphicon glyphicon-chevron-right\"></span></a></li><li><a href=\"/faq/\">Jobs<span class=\"glyphicon glyphicon-chevron-right\"></span></a></li><li><a href=\"/faq/\">Case Studies<span class=\"glyphicon glyphicon-chevron-right\"></span></a></li><li><a href=\"/faq/\">Blog<span class=\"glyphicon glyphicon-chevron-right\"></span></a></li><li><a href=\"/faq/\">Shop<span class=\"glyphicon glyphicon-chevron-right\"></span></a></li><li><a href=\"/faq/\">About<span class=\"glyphicon glyphicon-chevron-right\"></span></a></li></ul>";
    }

    var width = $(window).width();
    if (width <= 767) {
        showMobileTakeOver();
    }
    else if (width <= 970 && width >= 768) {
        showTabletTakeOver();
    }
    else if (width <= 1240 && width > 971) {
        showSmallDesktopTakeOver();
    }
    else if (width <= 1483 && width > 1241) {
        showSmallDesktopTakeOver();
    }
    else if (width > 1484) {
        showLargeDesktopTakeOver();
    }

    function showMobileTakeOver() {
        $("#takeOverTop").html("<div style='background:#fff;text-align:center;margin:0 auto;border-bottom:1px solid #d8d8d8;'><img src='mobile.gif'/></div>");
        $("#takeOverLeft").html("");
        $("#takeOverRight").html("");
        $("#mainContent").removeClass().addClass('container mobileContent');
        $("#takeOverMain").removeClass().addClass('container padding-left-right-0 mobileContent');
        $(".nav-tabs li a").attr("style", "padding:5px 15px 5px 15px!important;");
        $("#leaderboard").html("<div style='background:#fff;text-align:center;margin:0 auto;border-bottom:1px solid #d8d8d8;padding-bottom:15px;'><img src='mobile.gif'/></div>");
        $("#mostleft").removeClass().addClass("left hidden-md");
    }

    function showTabletTakeOver() {
        
        $("#takeOverTop").html("<div style='border-bottom:1px solid #d8d8d8;'><img src='ipadtop.gif' class='img-responsive'/></div>");
        $("#takeOverLeft").html("");
        $("#takeOverRight").html("");
        $("#mainContent").removeClass().addClass('container tabletContent');
        $("#takeOverMain").removeClass().addClass('container padding-left-right-0 tabletContent');
        $(".nav-tabs li a").attr("style", "padding:5px 15px 5px 15px!important;");
        $("#mostleft").removeClass().addClass("left hidden-md");
    }

    function showSmallDesktopTakeOver() {
        $("#takeOverTop").html("<div style='height:101px;border-bottom:1px solid #d8d8d8;'><embed width=\"970\" height=\"100\" src=\"topsmall.swf\"></div>");
        $("#takeOverLeft").html("<div style=\"position:absolute;left:-105px;top:0;\"><embed width=\"105\" height=\"818\" src=\"leftsmall.swf\"></div>");
        $("#takeOverRight").html("<div style=\"position:absolute;right:-105px;top:0;\"><embed width=\"105\" height=\"818\" src=\"rightsmall.swf\"></div>");
        $("#mainContent").removeClass().addClass('container smallDesktopContent');
        $("#takeOverMain").removeClass().addClass('container padding-left-right-0 smallDesktopContent');
        $(".nav-tabs li a").attr("style", "padding:5px 7px 5px 7px!important;");
        $("#mostleft").removeClass().addClass("left hidden-md hidden-lg");
    }

    function showLargeDesktopTakeOver() {
        $("#takeOverTop").html("<div style='height:101px;border-bottom:1px solid #d8d8d8;'><embed width=\"1170\" height=\"100\" src=\"top.swf\"></div>");
        $("#takeOverLeft").html("<div style=\"position:absolute;left:-167px;top:0;\"><embed width=\"167\" height=\"1054\" src=\"left.swf\"></div>");
        $("#takeOverRight").html("<div style=\"position:absolute;right:-167px;top:0;\"><embed width=\"167\" height=\"1054\" src=\"right.swf\"></div>");
        $("#mainContent").removeClass().addClass('container largeDesktopContent');
        $("#takeOverMain").removeClass().addClass('container padding-left-right-0 largeDesktopContent');
        $(".nav-tabs li a").attr("style", "padding:5px 15px 5px 15px!important;");
        $("#mostleft").removeClass().addClass("left hidden-md");
    }

    //left menu click
    $(document).on("click", "#menu", function (e) {
        e.preventDefault();
        var className = $("#sidr").attr("class");
        if (className.indexOf("open") >= 0) {
            $("#sidr").animate({
                left: "-260px",
                display: "none"
            }, 100);
            $("#mainContent").attr("style", "left:0;");
            $("#mainContent2").attr("style", "left:0;");
            $("#sidebarmenu").attr("style", "left:0;");
            $("#fixedTakeOver").attr("style", "left:0;");
            $("#fixedHeader").attr("style", "left:0;");
            $("#sidr").removeClass().addClass("sidr close");
        }
        else if (className.indexOf("close") >= 0) {
            if (width <= 970 && width >= 768) {
                $("#mainContent").attr("style", "left:250px;overflow-x:hidden;");
                $("#mainContent2").attr("style", "left:250px;overflow-x:hidden;");
            }
            else {
                $("#mainContent").attr("style", "left:260px;overflow-x:hidden;");
                $("#mainContent2").attr("style", "left:260px;overflow-x:hidden;");
            }
            $("#sidebarmenu").attr("style", "left:260px;");
            $("#fixedTakeOver").attr("style", "left:260px;");
            $("#fixedHeader").attr("style", "left:260px;");
            $("#sidr").removeClass().addClass("sidr open");
            $("#sidr").animate({
                left: "0",
                display: "block"
            }, 100);
        }

    });

    //top story click
    $(document).on("click", "#topstory", function (e) {
        e.preventDefault();
        var className = $("#rightsidebar").attr("class");
        if (className.indexOf("open") >= 0) {
            $("#rightsidebar").animate({
                right: "-260px",
                display: "none"
            }, 100);
            $("#mainContent").attr("style", "right:0;");
            $("#mainContent2").attr("style", "right:0;");
            $("#sidebarmenu").attr("style", "right:0;");
            $("#fixedTakeOver").attr("style", "right:0;");
            $("#fixedHeader").attr("style", "right:0;");
            $("#rightsidebar").removeClass().addClass("rightsidebar close");
        }
        else if (className.indexOf("close") >= 0) {
            if (width <= 970 && width >= 768) {
                $("#mainContent").attr("style", "right:250px;overflow-x:hidden;");
                $("#mainContent2").attr("style", "right:250px;overflow-x:hidden;");
            }
            else {
                $("#mainContent").attr("style", "right:260px;overflow-x:hidden;");
                $("#mainContent2").attr("style", "right:260px;overflow-x:hidden;");
            }
            $("#sidebarmenu").attr("style", "right:260px;");
            $("#fixedTakeOver").attr("style", "right:260px;");
            $("#fixedHeader").attr("style", "right:260px;");
            $("#rightsidebar").removeClass().addClass("rightsidebar open");
            $("#rightsidebar").animate({
                right: "0",
                display: "block"
            }, 100);
        }

    });
});